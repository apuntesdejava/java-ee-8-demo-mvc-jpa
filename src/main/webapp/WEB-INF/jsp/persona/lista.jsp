<%-- 
    Document   : lista
    Created on : May 25, 2016, 5:49:46 PM
    Author     : dsilva
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="/WEB-INF/jsp/includes/bootstrap.jspf" %>
    </head>
    <body>
        <div class="container">
            <h1>Personas</h1>
            <ul>
                <li><a href="<%= getServletContext().getContextPath()%>/main-app/persona/nuevo">Nuevo</a></li>
            </ul>
            <table class="table">
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Fecha nacimiento</th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <c:forEach items="${personas}" var="item">
                        <tr>
                            <td>${item.id}</td>
                            <td>${item.nombre}</td>
                            <td><fmt:formatDate value="${item.fechaNacimiento}" pattern="dd/MM/yyyy" /></td>
                            <td>
                                [<a href="<%= getServletContext().getContextPath()%>/main-app/persona/editar/${item.id}">Editar</a>]
                                [<a onclick="return confirm('¿Está seguro de borrar este registro?')" href="<%= getServletContext().getContextPath()%>/main-app/persona/borrar/${item.id}">Borrar</a>]
                            </td>
                        </tr>
                    </c:forEach>
                </tbody>
            </table>
        </div>
    </body>
</html>
