<%-- 
    Document   : form
    Created on : May 25, 2016, 6:23:58 PM
    Author     : dsilva
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
        <%@include file="/WEB-INF/jsp/includes/bootstrap.jspf" %>
    </head>
    <body>
        <div class="container">
            <h1>Persona</h1>
            <form method="POST" action="<%= getServletContext().getContextPath()%>/main-app/persona">                
                <input name="id" type="hidden" value="${persona.id}" />
                <div class="form-group">
                    <label>Nombre</label>
                    <input name="nombre" class="form-control" type="text" value="${persona.nombre}"/>
                </div>
                <div class="form-group">
                    <label>Fecha nacimiento</label>
                    <input name="fecha_nacimiento" class="form-control" type="date" value="${persona.fechaNacimientoString}"/><br/>
                </div>
                <button type="submit" class="btn btn-default">Guardar</button>
            </form>
        </div>
    </body>
</html>
