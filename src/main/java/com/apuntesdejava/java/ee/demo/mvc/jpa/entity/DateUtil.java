/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apuntesdejava.java.ee.demo.mvc.jpa.entity;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author diego.silva@apuntesdejava.com
 */
public class DateUtil {

    static final DateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");
    private static final Logger LOG = Logger.getLogger(DateUtil.class.getName());

    public static Date parse(String date) {
        if (date != null && !date.isEmpty()) {
            try {
                return DATE_FORMAT.parse(date);
            } catch (ParseException ex) {
                LOG.log(Level.SEVERE, null, ex);
            }
        }
        return null;
    }

    public static String format(Date date) {
        if (date == null) {
            return null;
        }
        return DATE_FORMAT.format(date);
    }
}
