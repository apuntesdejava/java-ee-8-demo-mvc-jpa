/*
 * Copyright 2016 dsilva.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.apuntesdejava.java.ee.demo.mvc.jpa.controller;

import com.apuntesdejava.java.ee.demo.mvc.jpa.entity.Persona;
import com.apuntesdejava.java.ee.demo.mvc.jpa.service.PersonaFacade;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.mvc.Controller;
import javax.mvc.Models;
import javax.mvc.View;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;

/**
 *
 * @author dsilva
 */
@Path("/persona")
@Controller
@RequestScoped
public class PersonaController {

    private static final Logger LOG = Logger.getLogger(PersonaController.class.getName());

    @Inject
    private PersonaFacade personaFacade;
    @Inject
    private Models models;

    @GET
    @View("/WEB-INF/jsp/persona/lista.jsp")
    public void index() {
        LOG.info("Obteniendo lista de personas");
        List<Persona> personas = personaFacade.findAll();
        LOG.log(Level.INFO, "Total obtenidos:{0}", personas.size());
        models.put("personas", personas);

    }

    @POST
    @View("/WEB-INF/jsp/persona/lista.jsp")
    public void actualizar(@BeanParam Persona persona) {
        if (persona.getId() == null || persona.getId() == 0) {
            personaFacade.create(persona);
        } else {
            personaFacade.edit(persona);
        }
        LOG.log(Level.INFO, "Persona creada:{0}", persona);
        index();
    }

    @GET
    @Path("borrar/{id}")
    @View("/WEB-INF/jsp/persona/lista.jsp")
    public void borrar(@PathParam("id") Long id) {
        Persona persona = personaFacade.find(id);
        personaFacade.remove(persona);
        LOG.log(Level.INFO, "Persona borrada:{0}", persona);
        index();
    }

    @GET
    @Path("editar/{id}")
    @View("/WEB-INF/jsp/persona/form.jsp")
    public void editar(@PathParam("id") Long id) {
        Persona persona = personaFacade.find(id);
        models.put("persona", persona);
        LOG.log(Level.INFO, "Persona a editar:{0}", persona);
    }

    @GET
    @Path("nuevo")
    public String nuevo() {
        return "/WEB-INF/jsp/persona/form.jsp";
    }

}
